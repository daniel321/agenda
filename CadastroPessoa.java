import java.io.*;

public class CadastroPessoa{

  public static void main(String[] args) throws IOException{
    //burocracia para leitura de teclado
    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;

    //instanciando objetos do sistema
    ControlePessoa umControle = new ControlePessoa();
    Pessoa umaPessoa = new Pessoa();

    //interagindo com usuário
    System.out.println("Digite o nome da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umNome = entradaTeclado;
    umaPessoa.setNome(umNome);

    System.out.println("Digite o telefone da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umTelefone = entradaTeclado;
    umaPessoa.setTelefone(umTelefone);

    System.out.println("Digite a idade da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    String umaIdade = entradaTeclado;
    umaPessoa.setIdade(umaIdade);

    System.out.println("Digite o sexo da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    umaPessoa.setSexo(entradaTeclado);

    System.out.println("Digite o endereço da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    umaPessoa.setEndereço(entradaTeclado);

    System.out.println("Digite o endereço de e-mail da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    umaPessoa.setEmail(entradaTeclado);

    System.out.println("Digite o RG da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    umaPessoa.setRG(entradaTeclado);

    System.out.println("Digite o CPF da Pessoa:");
    entradaTeclado = leitorEntrada.readLine();
    umaPessoa.setCpf(entradaTeclado);



    //adicionando uma pessoa na lista de pessoas do sistema
    String mensagem = umControle.adicionar(umaPessoa);

    //conferindo saída
    System.out.println("=================================");
    System.out.println(mensagem);
    System.out.println("=)");

  }

}