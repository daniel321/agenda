import java.util.ArrayList;

public class ControlePessoa {

//atributos

    private ArrayList<Pessoa> listaPessoas;

//construtor

    public ControlePessoa() {
        listaPessoas = new ArrayList<Pessoa>();
    }

// métodos

    public String adicionar(Pessoa umaPessoa) {
        String mensagem = umPessoa.nome + " foi adicionado(a) com Sucesso!";
        listaPessoas.add(umaPessoa);
        return mensagem;
    }



    public String remover(Pessoa umPessoa) {
        listaPessoas.remove(umPessoa);
        String mensagem = umPessoa.nome + " foi removido(a) com Sucesso!";
        return mensagem;
    }


/*
    public Pessoa pesquisar(String nome) {
        for (Pessoa pessoa: listaPessoas) {
            if (pessoa.getNome().equalsIgnoreCase(nome)) return pessoa;
        }
        return null;
    }
*/
}
